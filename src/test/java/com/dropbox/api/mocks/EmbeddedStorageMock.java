package com.dropbox.api.mocks;

import com.dropbox.api.models.User;
import com.dropbox.api.models.UserFilesStorage;
import com.dropbox.api.storage.Storage;

import java.util.Map;
import java.util.Optional;

public class EmbeddedStorageMock implements Storage {
    private final Map<String, User> users;
    private final Map<String, UserFilesStorage> usersStorages;

    public EmbeddedStorageMock(Map<String, User> users, Map<String, UserFilesStorage> usersStorages) {
        this.users = users;
        this.usersStorages = usersStorages;
    }

    @Override
    public boolean userExists(String email) {
        return users.values().stream().anyMatch(user -> user.getEmail().equals(email));
    }

    @Override
    public Optional<User> getUserById(String userId) {
        return Optional.ofNullable(users.get(userId));
    }

    @Override
    public User saveUser(User user) {
        users.put(user.getUserId(), user);
        return user;
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        return users.values().stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst();
    }

    @Override
    public Optional<UserFilesStorage> findUserStorage(String userId) {
        return Optional.ofNullable(usersStorages.get(userId));
    }

    @Override
    public UserFilesStorage saveUserStorage(UserFilesStorage storage) {
        usersStorages.put(storage.getUserId(), storage);
        return storage;
    }
}
