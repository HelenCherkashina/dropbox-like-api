package com.dropbox.api.services;

import com.dropbox.api.mocks.EmbeddedStorageMock;
import com.dropbox.api.models.File;
import com.dropbox.api.models.FileRequest;
import com.dropbox.api.models.Permission;
import com.dropbox.api.models.User;
import com.dropbox.api.models.UserFilesStorage;
import com.dropbox.api.storage.Storage;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dropbox.api.models.FileRequest.createFileRequest;
import static com.dropbox.api.models.FileType.FOLDER;
import static com.dropbox.api.models.Permission.UPDATE;
import static com.dropbox.api.models.Permission.VIEW;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static junit.framework.TestCase.assertEquals;

public class ShareFilesServiceFunctionalTest {
    private UsersService usersService;
    private UserFilesStorageService userFilesStorageService;
    private ShareFilesService shareFilesService;

    private UserFilesStorage defaultUserStorage;

    @Before
    public void setUp() {
        Storage storage = new EmbeddedStorageMock(new HashMap<>(), new HashMap<>());
        this.usersService = new UserServiceImpl(storage);
        this.userFilesStorageService = new UserFilesStorageServiceImpl(storage);
        this.shareFilesService = new ShareFilesServiceImpl(userFilesStorageService, usersService);

        User defaultUser = usersService.createUser("default@test.test", "password");
        this.defaultUserStorage = userFilesStorageService.createUserStorage(defaultUser.getUserId());
    }

    @Test
    public void shouldAssignPermissionsToDocument() {
        //given
        User user = usersService.createUser("test@test.test", "password");

        FileRequest folderRequest = createFileRequest(FOLDER, "testFolder", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(),
                defaultUserStorage.getRootFolder().getId(), singletonList(folderRequest));
        File testFolder = folder.searchByName("testFolder").get(0);

        Map<String, List<Permission>> permissions = new HashMap<>();
        permissions.put(user.getEmail(), asList(VIEW, UPDATE));

        //when
        shareFilesService.assignPermissionsToResource(defaultUserStorage.getUserId(), testFolder.getId(), permissions);

        //then
        testFolder = folder.searchByName("testFolder").get(0);
        assertEquals(1, testFolder.getUsersToShare().size());
        assertEquals(asList(VIEW, UPDATE), testFolder.getUsersToShare().get(user.getUserId()));
    }

    @Test
    public void shouldRemoveUsersToShare() {
        //given
        User user1 = usersService.createUser("test1@test.test", "password");
        User user2 = usersService.createUser("test2@test.test", "password");

        Map<String, List<Permission>> permissions = new HashMap<>();
        permissions.put(user1.getUserId(), asList(VIEW, UPDATE));
        permissions.put(user2.getUserId(), singletonList(VIEW));

        FileRequest folderRequest = createFileRequest(FOLDER, "testFolder", permissions);
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(),
                defaultUserStorage.getRootFolder().getId(), singletonList(folderRequest));
        File testFolder = folder.searchByName("testFolder").get(0);

        //when
        shareFilesService.removeUsersToShare(defaultUserStorage.getUserId(), testFolder.getId(), singletonList(user2.getUserId()));

        //then
        testFolder = folder.searchByName("testFolder").get(0);
        assertEquals(1, testFolder.getUsersToShare().size());
        assertEquals(asList(VIEW, UPDATE), testFolder.getUsersToShare().get(user1.getUserId()));
    }

    @Test
    public void shouldRemoveUserPermissions() {
        //given
        User user1 = usersService.createUser("test1@test.test", "password");
        User user2 = usersService.createUser("test2@test.test", "password");

        Map<String, List<Permission>> permissions = new HashMap<>();
        permissions.put(user1.getUserId(), new ArrayList<>(asList(VIEW, UPDATE)));
        permissions.put(user2.getUserId(), new ArrayList<>(asList(VIEW, UPDATE)));

        FileRequest folderRequest = createFileRequest(FOLDER, "testFolder", permissions);
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(),
                defaultUserStorage.getRootFolder().getId(), singletonList(folderRequest));
        File testFolder = folder.searchByName("testFolder").get(0);

        //when
        shareFilesService.removePermissionsForUser(defaultUserStorage.getUserId(), testFolder.getId(),
                user1.getUserId(), singletonList(UPDATE));

        //then
        testFolder = folder.searchByName("testFolder").get(0);
        assertEquals(2, testFolder.getUsersToShare().size());
        assertEquals(singletonList(VIEW), testFolder.getUsersToShare().get(user1.getUserId()));
        assertEquals(asList(VIEW, UPDATE), testFolder.getUsersToShare().get(user2.getUserId()));
    }
}