package com.dropbox.api.services;

import com.dropbox.api.exceptions.FileNotFoundException;
import com.dropbox.api.mocks.EmbeddedStorageMock;
import com.dropbox.api.models.File;
import com.dropbox.api.models.FileRequest;
import com.dropbox.api.models.User;
import com.dropbox.api.models.UserFilesStorage;
import com.dropbox.api.storage.Storage;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.dropbox.api.models.File.createNewFile;
import static com.dropbox.api.models.FileRequest.createFileRequest;
import static com.dropbox.api.models.FileType.DOCUMENT;
import static com.dropbox.api.models.FileType.FOLDER;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotEquals;

public class UsersStorageServiceFunctionalTest {
    private UsersService usersService;
    private UserFilesStorageService userFilesStorageService;

    private UserFilesStorage defaultUserStorage;

    @Before
    public void setUp() {
        Storage storage = new EmbeddedStorageMock(new HashMap<>(), new HashMap<>());
        this.usersService = new UserServiceImpl(storage);
        this.userFilesStorageService = new UserFilesStorageServiceImpl(storage);

        User defaultUser = usersService.createUser("default@test.test", "password");
        this.defaultUserStorage = userFilesStorageService.createUserStorage(defaultUser.getUserId());
    }

    @Test
    public void shouldCreateUserStorage() {
        //given

        //when
        UserFilesStorage userStorage = userFilesStorageService.createUserStorage(defaultUserStorage.getUserId());

        //then
        assertNotNull(userStorage);
        assertNotNull(userStorage.getRootFolder());
        assertEquals("Dropbox", userStorage.getRootFolder().getName().getUploadedName());
        assertEquals(userStorage.getRootFolder().getName().getUploadedName(),
                userStorage.getRootFolder().getName().getGeneratedName());
        assertNull(userStorage.getRootFolder().getParentFileId());
        assertEquals(new ArrayList<>(), userStorage.getRootFolder().getFiles());
        assertEquals(1, userStorage.getStorageFiles().size());
        assertTrue(userStorage.getStorageFiles().containsKey(userStorage.getRootFolder().getId()));
    }

    @Test
    public void shouldFindUserStorage() {
        //given

        //when
        UserFilesStorage userStorage = userFilesStorageService.findOrCreateUserFilesStorage(defaultUserStorage.getUserId());

        //then
        assertNotNull(userStorage);
        assertNotNull(userStorage.getRootFolder());
        assertEquals("Dropbox", userStorage.getRootFolder().getName().getUploadedName());
        assertEquals(userStorage.getRootFolder().getName().getUploadedName(),
                userStorage.getRootFolder().getName().getGeneratedName());
        assertNull(userStorage.getRootFolder().getParentFileId());
        assertEquals(new ArrayList<>(), userStorage.getRootFolder().getFiles());
        assertEquals(1, userStorage.getStorageFiles().size());
        assertTrue(userStorage.getStorageFiles().containsKey(userStorage.getRootFolder().getId()));
    }

    @Test
    public void shouldAddEmptyFolderToRoot() {
        //given
        FileRequest folderRequest = createFileRequest(FOLDER, "testFolder", new HashMap<>());

        //when
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(),
                defaultUserStorage.getRootFolder().getId(), singletonList(folderRequest));

        //then
        assertEquals("Dropbox", folder.getName().getGeneratedName());
        assertEquals(1, folder.getFiles().size());
        assertEquals(FOLDER, folder.getFiles().get(0).getType());
        assertEquals("testFolder", folder.getFiles().get(0).getName().getGeneratedName());
        assertEquals("testFolder", folder.getFiles().get(0).getName().getUploadedName());
    }

    @Test
    public void shouldAddFileToFolder() {
        //given
        FileRequest parentFolderRequest = createFileRequest(FOLDER, "parentFolder", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                singletonList(parentFolderRequest));
        FileRequest documentRequest = createFileRequest(DOCUMENT, "testDocument", new HashMap<>());
        FileRequest folderRequest = createFileRequest(FOLDER, "testFolder", new HashMap<>());
        List<File> folders = folder.searchByName("parentFolder");

        //when
        File parentFolder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), folders.get(0).getId(),
                asList(documentRequest,
                        folderRequest));

        //then
        assertEquals("parentFolder", parentFolder.getName().getUploadedName());
        assertEquals(FOLDER, parentFolder.getType());
        assertNotNull(parentFolder.getFiles());
        assertEquals(2, parentFolder.getFiles().size());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldThrowExceptionIfNotAFolder() {
        //given
        File testDocument = createNewFile(defaultUserStorage.getRootFolder().getId(), DOCUMENT, "testDocument", new ArrayList<>(),
                new HashMap<>());
        File parentFolder = defaultUserStorage.addFiles(defaultUserStorage.getRootFolder().getId(), singletonList(testDocument));
        List<File> documents = parentFolder.searchByName("testDocument");

        //when
        userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), documents.get(0).getId(), new ArrayList<>());

        //then
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowExceptionIfFileNotFound() {
        //given

        //when
        userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), "wrong", new ArrayList<>());

        //then
    }

    @Test
    public void shouldRenameFile() {
        //given
        FileRequest folderRequest = createFileRequest(FOLDER, "testFolder", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                singletonList(folderRequest));
        List<File> folders = folder.searchByName("testFolder");

        //when
        File parentFolder = userFilesStorageService.renameFile(defaultUserStorage.getUserId(), folders.get(0).getId(), "newName");

        //then
        assertEquals("newName", parentFolder.getName().getUploadedName());
        assertEquals("newName", parentFolder.getName().getGeneratedName());
    }

    @Test
    public void shouldRenameFilesAutomaticallyWhileAdding() {
        //given
        FileRequest parentFolderRequest = createFileRequest(FOLDER, "testFolder", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                singletonList(parentFolderRequest));
        FileRequest documentRequest = createFileRequest(DOCUMENT, "testDocument", new HashMap<>());
        FileRequest folderRequest = createFileRequest(DOCUMENT, "testDocument", new HashMap<>());
        List<File> folders = folder.searchByName("testFolder");

        //when
        File parentFolder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), folders.get(0).getId(),
                asList(documentRequest, folderRequest));

        //then
        assertEquals("testFolder", parentFolder.getName().getUploadedName());
        assertEquals(FOLDER, parentFolder.getType());
        assertNotNull(parentFolder.getFiles());
        assertEquals(2, parentFolder.getFiles().size());
        assertEquals("testDocument", parentFolder.getFiles().get(0).getName().getUploadedName());
        assertEquals("testDocument", parentFolder.getFiles().get(0).getName().getGeneratedName());
        assertEquals("testDocument", parentFolder.getFiles().get(1).getName().getUploadedName());
        assertEquals("testDocument(1)", parentFolder.getFiles().get(1).getName().getGeneratedName());
    }

    @Test
    public void shouldAddFileAndFolderWithTheSameNames() {
        //given
        FileRequest parentFolderRequest = createFileRequest(FOLDER, "testFolder", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                singletonList(parentFolderRequest));
        FileRequest documentRequest = createFileRequest(DOCUMENT, "testDocument", new HashMap<>());
        FileRequest folderRequest = createFileRequest(FOLDER, "testDocument", new HashMap<>());
        List<File> folders = folder.searchByName("testFolder");

        //when
        File parentFolder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), folders.get(0).getId(),
                asList(documentRequest, folderRequest));

        //then
        assertEquals("testFolder", parentFolder.getName().getUploadedName());
        assertEquals(FOLDER, parentFolder.getType());
        assertNotNull(parentFolder.getFiles());
        assertEquals(2, parentFolder.getFiles().size());
        assertEquals(DOCUMENT, parentFolder.getFiles().get(0).getType());
        assertEquals("testDocument", parentFolder.getFiles().get(0).getName().getUploadedName());
        assertEquals("testDocument", parentFolder.getFiles().get(0).getName().getGeneratedName());
        assertEquals(FOLDER, parentFolder.getFiles().get(1).getType());
        assertEquals("testDocument", parentFolder.getFiles().get(1).getName().getUploadedName());
        assertEquals("testDocument", parentFolder.getFiles().get(1).getName().getGeneratedName());
    }

    @Test
    public void shouldAddFilesTheSameNamesToDifferentFolders() {
        //given
        FileRequest testFolderRequest1 = createFileRequest(FOLDER, "testFolder1", new HashMap<>());
        FileRequest testFolderRequest2 = createFileRequest(FOLDER, "testFolder2", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                asList(testFolderRequest1, testFolderRequest2));
        FileRequest documentRequest = createFileRequest(DOCUMENT, "testDocument", new HashMap<>());
        File testFolder1 = folder.searchByName("testFolder1").get(0);
        File testFolder2 = folder.searchByName("testFolder2").get(0);

        //when
        File folder1 = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), testFolder1.getId(),
                singletonList(documentRequest));
        File folder2 = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), testFolder2.getId(),
                singletonList(documentRequest));

        //then
        assertNotNull(folder1.getFiles());
        assertEquals(1, folder1.getFiles().size());
        assertEquals(DOCUMENT, folder1.getFiles().get(0).getType());
        assertEquals("testDocument", folder1.getFiles().get(0).getName().getUploadedName());
        assertEquals("testDocument", folder1.getFiles().get(0).getName().getGeneratedName());

        assertNotNull(folder2.getFiles());
        assertEquals(1, folder2.getFiles().size());
        assertEquals(DOCUMENT, folder2.getFiles().get(0).getType());
        assertEquals("testDocument", folder2.getFiles().get(0).getName().getUploadedName());
        assertEquals("testDocument", folder2.getFiles().get(0).getName().getGeneratedName());
    }

    @Test
    public void shouldMoveFileToAnotherFolder() {
        //given
        FileRequest testFolderRequest1 = createFileRequest(FOLDER, "testFolder1", new HashMap<>());
        FileRequest testFolderRequest2 = createFileRequest(FOLDER, "testFolder2", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                asList(testFolderRequest1, testFolderRequest2));
        FileRequest documentRequest = createFileRequest(DOCUMENT, "testDocument", new HashMap<>());
        File testFolder1 = folder.searchByName("testFolder1").get(0);
        File testFolder2 = folder.searchByName("testFolder2").get(0);
        userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), testFolder1.getId(),
                singletonList(documentRequest));
        File testDocument = testFolder1.searchByName("testDocument").get(0);

        //when
        File file = userFilesStorageService.moveFile(defaultUserStorage.getUserId(), testDocument.getId(), testFolder2.getId());

        //then
        assertNotEquals(testDocument.getId(), file.getId());
        assertEquals(0, testFolder1.searchByName("testDocument").size());
        assertEquals(1, testFolder2.searchByName("testDocument").size());
        assertEquals(testFolder2.getId(), file.getParentFileId());
    }

    @Test
    public void shouldCopyFileToAnotherFolder() {
        //given
        FileRequest testFolderRequest1 = createFileRequest(FOLDER, "testFolder1", new HashMap<>());
        FileRequest testFolderRequest2 = createFileRequest(FOLDER, "testFolder2", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                asList(testFolderRequest1, testFolderRequest2));
        FileRequest documentRequest = createFileRequest(DOCUMENT, "testDocument", new HashMap<>());
        File testFolder1 = folder.searchByName("testFolder1").get(0);
        File testFolder2 = folder.searchByName("testFolder2").get(0);
        userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), testFolder1.getId(),
                singletonList(documentRequest));
        File testDocument = testFolder1.searchByName("testDocument").get(0);

        //when
        File file = userFilesStorageService.copyFile(defaultUserStorage.getUserId(), testDocument.getId(), testFolder2.getId());

        //then
        assertNotEquals(testDocument.getId(), file.getId());
        assertEquals(1, testFolder1.searchByName("testDocument").size());
        assertEquals(1, testFolder2.searchByName("testDocument").size());
        assertEquals(testFolder2.getId(), file.getParentFileId());
    }

    @Test
    public void shouldRemoveFiles() {
        //given
        FileRequest testFolderRequest = createFileRequest(FOLDER, "testFolder", new HashMap<>());
        File folder = userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), defaultUserStorage.getRootFolder().getId(),
                singletonList(testFolderRequest));
        FileRequest documentRequest1 = createFileRequest(DOCUMENT, "testDocument1", new HashMap<>());
        FileRequest documentRequest2 = createFileRequest(DOCUMENT, "notRemovable", new HashMap<>());
        FileRequest documentRequest3 = createFileRequest(DOCUMENT, "testDocument2", new HashMap<>());
        FileRequest documentRequest4 = createFileRequest(DOCUMENT, "testDocument3", new HashMap<>());
        File testFolder = folder.searchByName("testFolder").get(0);
        userFilesStorageService.addFilesToFolder(defaultUserStorage.getUserId(), testFolder.getId(),
                asList(documentRequest1, documentRequest2, documentRequest3, documentRequest4));
        List<String> documentsIds = testFolder.searchByName("testDocument").stream().map(File::getId).collect(Collectors.toList());

        //when
        userFilesStorageService.removeFiles(defaultUserStorage.getUserId(), documentsIds);

        //then
        testFolder = folder.searchByName("testFolder").get(0);
        assertEquals(1, testFolder.getFiles().size());
        assertEquals("notRemovable", testFolder.getFiles().get(0).getName().getGeneratedName());
    }
}