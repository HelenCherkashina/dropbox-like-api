package com.dropbox.api.services;

import com.dropbox.api.exceptions.InvalidCredentialsException;
import com.dropbox.api.exceptions.UserAlreadyExistsException;
import com.dropbox.api.exceptions.UserNotFoundException;
import com.dropbox.api.mocks.EmbeddedStorageMock;
import com.dropbox.api.models.User;
import com.dropbox.api.storage.Storage;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;

public class UsersServiceFunctionalTest {
    private UsersService usersService;

    private User defaultUser;

    @Before
    public void setUp() {
        Storage storage = new EmbeddedStorageMock(new HashMap<>(), new HashMap<>());
        this.usersService = new UserServiceImpl(storage);
        this.defaultUser = usersService.createUser("default@test.test", "password");
    }

    @Test
    public void shouldFindUserById() {
        //given

        //when
        User user = usersService.findUserById(defaultUser.getUserId());

        //then
        assertEquals(defaultUser.getEmail(), user.getEmail());
    }

    @Test(expected = UserNotFoundException.class)
    public void shouldThrowExceptionIfUserIsNotFound() {
        //given

        //when
        User user = usersService.findUserById("notSearchable");

        //then
    }

    @Test
    public void shouldFindUserByEmail() {
        //given

        //when
        User user = usersService.findUserByEmail(defaultUser.getEmail());

        //then
        assertEquals(defaultUser.getEmail(), user.getEmail());
    }

    @Test
    public void shouldCreateNewUser() {
        //given

        //when
        User user = usersService.createUser("test@test.test", "password");

        //then
        assertEquals("test@test.test", user.getEmail());
        assertEquals("password", user.getPassword());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void shouldThrowAnExceptionIfUserWithGivenEmailExists() {
        //given

        //when
        User user = usersService.createUser("default@test.test", "password");

        //then
    }

    @Test
    public void shouldAuthenticateUser() {
        //given

        //when
        User user = usersService.authenticateUser("default@test.test", "password");

        //then
        assertEquals(defaultUser.getEmail(), user.getEmail());
    }

    @Test(expected = InvalidCredentialsException.class)
    public void shouldThrowAnExceptionIfUserEnteredInvalidCredentials() {
        //given

        //when
        User user = usersService.authenticateUser("default@test.test", "wrong");

        //then
        assertEquals(defaultUser.getEmail(), user.getEmail());
    }
}