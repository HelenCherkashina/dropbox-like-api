package com.dropbox.api.storage;

import com.dropbox.api.models.User;
import com.dropbox.api.models.UserFilesStorage;

import java.util.Optional;

public interface Storage {
    boolean userExists(String email);

    Optional<User> getUserById(String userId);

    User saveUser(User user);

    UserFilesStorage saveUserStorage(UserFilesStorage storage);

    Optional<User> findUserByEmail(String email);

    Optional<UserFilesStorage> findUserStorage(String userId);
}
