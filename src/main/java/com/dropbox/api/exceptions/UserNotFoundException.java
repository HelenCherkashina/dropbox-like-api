package com.dropbox.api.exceptions;

/**
 * Thrown to indicate that the requested user doesn't exist
 */
public class UserNotFoundException extends RuntimeException {
}
