package com.dropbox.api.exceptions;

/**
 * Thrown to indicate that the user provided invalid credentials
 */
public class InvalidCredentialsException extends RuntimeException {
}
