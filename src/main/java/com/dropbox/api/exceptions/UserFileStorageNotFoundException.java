package com.dropbox.api.exceptions;

/**
 * Thrown to indicate that the no user's file store exist
 */
public class UserFileStorageNotFoundException extends Exception {
}
