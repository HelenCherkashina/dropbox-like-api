package com.dropbox.api.exceptions;

/**
 * Thrown to indicate that the requested file doesn't exist
 */
public class FileNotFoundException extends RuntimeException {
}
