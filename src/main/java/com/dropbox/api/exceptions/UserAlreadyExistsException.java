package com.dropbox.api.exceptions;

/**
 * Thrown to indicate that the requested user already exists in the system
 */
public class UserAlreadyExistsException extends RuntimeException {
}
