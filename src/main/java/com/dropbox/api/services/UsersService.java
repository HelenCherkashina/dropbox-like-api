package com.dropbox.api.services;

import com.dropbox.api.models.User;

/**
 * Service for managing users
 */
public interface UsersService {
    /**
     * Search for the user with the given id
     *
     * @param userId user identifier
     * @return user
     */
    User findUserById(String userId);

    /**
     * Search for the user with the given email
     *
     * @param email user email
     * @return user
     */
    User findUserByEmail(String email);

    /**
     * Create a new user
     *
     * @param email user email
     * @param password user password
     * @return created user
     */
    User createUser(String email, String password);

    /**
     * Authenticate user by email and password
     *
     * @param email user email
     * @param password user password
     * @return existing user
     */
    User authenticateUser(String email, String password);
}
