package com.dropbox.api.services;

import com.dropbox.api.models.File;
import com.dropbox.api.models.FileRequest;
import com.dropbox.api.models.UserFilesStorage;

import java.util.List;

/**
 * Service to manipulate user file storage
 */
public interface UserFilesStorageService {
    /**
     * Create a new empty user storage
     *
     * @param userId user identifier
     * @return create user storage
     */
    UserFilesStorage createUserStorage(String userId);

    /**
     * Searches for the user storage or if it doesn't exist create new empty
     *
     * @param userId user identifier
     * @return user's files storage
     */
    UserFilesStorage findOrCreateUserFilesStorage(String userId);

    /**
     * Add files to the given folder
     *
     * @param userId user identifier
     * @param folderId folder identifier to add the files
     * @param fileRequests files requests for adding
     * @return owner folder of the given added files
     */
    File addFilesToFolder(String userId, String folderId, List<FileRequest> fileRequests);

    /**
     * Rename a file
     *
     * @param userId user identifier
     * @param fileId file identifier to rename
     * @param newName new name for the file
     * @return renamed file
     */
    File renameFile(String userId, String fileId, String newName);

    /**
     * Move file to another folder
     *
     * @param userId user identifier
     * @param fileId file identifier to move
     * @param toFolderId folder where to move the file
     * @return moved file
     */
    File moveFile(String userId, String fileId, String toFolderId);

    /**
     * Copy file to another folder
     *
     * @param userId user identifier
     * @param fileId file identifier to copy
     * @param toFolderId folder where to copy the file
     * @return copied file
     */
    File copyFile(String userId, String fileId, String toFolderId);

    /**
     * Remove files for the user
     *
     * @param userId user identifier
     * @param fileIds files identifiers to remove
     */
    void removeFiles(String userId, List<String> fileIds);

    /**
     * Save user's file storage
     *
     * @param userFilesStorage storage to save
     */
    void save(UserFilesStorage userFilesStorage);
}
