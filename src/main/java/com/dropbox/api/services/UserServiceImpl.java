package com.dropbox.api.services;

import com.dropbox.api.exceptions.InvalidCredentialsException;
import com.dropbox.api.exceptions.UserAlreadyExistsException;
import com.dropbox.api.exceptions.UserNotFoundException;
import com.dropbox.api.models.User;
import com.dropbox.api.storage.Storage;

import static com.dropbox.api.models.User.createNewUser;

/**
 * Service to handle users
 */
public class UserServiceImpl implements UsersService {
    private Storage storage;

    public UserServiceImpl(Storage storage) {
        this.storage = storage;
    }

    /**
     * Search for the user with the given id
     *
     * @param userId user identifier
     * @return user
     *
     * @throws UserNotFoundException if no user exist with the given id
     */
    @Override
    public User findUserById(String userId) {
        return storage.getUserById(userId).orElseThrow(UserNotFoundException::new);
    }

    /**
     * Search for the user with the given email
     *
     * @param email user email
     * @return user
     *
     * @throws UserNotFoundException if no user exist with the given email
     */
    @Override
    public User findUserByEmail(String email) {
        return storage.findUserByEmail(email)
                .orElseThrow(UserNotFoundException::new);
    }

    /**
     * Create a new user
     *
     * @param email user email
     * @param password user password
     * @return created user
     *
     * @throws UserAlreadyExistsException if user with the given email already exists
     */
    @Override
    public User createUser(String email, String password) {
        if (storage.userExists(email)) {
            throw new UserAlreadyExistsException();
        }

        return storage.saveUser(createNewUser(email, password));
    }

    /**
     * Authenticate user by email and password
     *
     * @param email user email
     * @param password user password
     * @return existing user
     *
     * @throws UserNotFoundException if no user exist with the given email
     * @throws InvalidCredentialsException if user provided invalid credentials
     */
    @Override
    public User authenticateUser(String email, String password) {
        User user = storage.findUserByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        if(user.getPassword().equals(password)) {
            return user;
        }

        throw new InvalidCredentialsException();
    }
}
