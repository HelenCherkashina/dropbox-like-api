package com.dropbox.api.services;

import com.dropbox.api.models.Permission;

import java.util.List;
import java.util.Map;

/**
 * Service to share resources within users
 */
public interface ShareFilesService {
    /**
     * Assign permissions to the given resource
     *
     * @param userId user identifier the file belongs to
     * @param fileId file identifier to share
     * @param usersPermissions users permissions to assign
     */
    void assignPermissionsToResource(String userId,
                                     String fileId,
                                     Map<String, List<Permission>> usersPermissions);

    /**
     * Remove file access from the given users
     *
     * @param userId user identifier the file belongs to
     * @param fileId file identifier to remove users access
     * @param usersIds users to remove access of the file
     */
    void removeUsersToShare(String userId, String fileId, List<String> usersIds);

    /**
     * Remove permissions from the given file for the given user
     *
     * @param userId user identifier the file belongs to
     * @param fileId file identifier to remove the given users permissions
     * @param userIdForPermissions user to remove permissions
     * @param permissions permissions to remove
     */
    void removePermissionsForUser(String userId, String fileId,
                                  String userIdForPermissions, List<Permission> permissions);
}
