package com.dropbox.api.services;

import com.dropbox.api.exceptions.FileNotFoundException;
import com.dropbox.api.models.File;
import com.dropbox.api.models.Permission;
import com.dropbox.api.models.UserFilesStorage;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service to share files within users
 */
public class ShareFilesServiceImpl implements ShareFilesService {
    private final UserFilesStorageService userFilesStorageService;
    private final UsersService usersService;

    public ShareFilesServiceImpl(UserFilesStorageService userFilesStorageService, UsersService usersService) {
        this.userFilesStorageService = userFilesStorageService;
        this.usersService = usersService;
    }

    /**
     * Assign permissions to the given file
     *
     * @param userId user identifier the file belongs to
     * @param fileId file identifier to share
     * @param usersPermissions users permissions to assign
     *
     * @throws FileNotFoundException if the given file doesn't exist
     */
    @Override
    public void assignPermissionsToResource(String userId, String fileId, Map<String, List<Permission>> usersPermissions) {
        UserFilesStorage userStorage = userFilesStorageService.findOrCreateUserFilesStorage(userId);
        Map<String, List<Permission>> permissions = usersPermissions.entrySet().stream()
                .collect(Collectors.toMap(entry -> usersService.findUserByEmail(entry.getKey()).getUserId(), Map.Entry::getValue));
        File file = userStorage.findFileByFileId(fileId).orElseThrow(FileNotFoundException::new);
        permissions.forEach(file::addUserToShare);
        userFilesStorageService.save(userStorage);
    }

    /**
     * Remove file access from the given users
     *
     * @param userId user identifier the file belongs to
     * @param fileId file identifier to remove users access
     * @param usersIds users to remove access of the file
     *
     * @throws FileNotFoundException if the given file doesn't exist
     */
    @Override
    public void removeUsersToShare(String userId, String fileId, List<String> usersIds) {
        UserFilesStorage userStorage = userFilesStorageService.findOrCreateUserFilesStorage(userId);
        File file = userStorage.findFileByFileId(fileId).orElseThrow(FileNotFoundException::new);
        file.removeUsersToShare(usersIds);
        userFilesStorageService.save(userStorage);
    }

    /**
     * Remove permissions from the given file for the given user
     *
     * @param userId user identifier the file belongs to
     * @param fileId file identifier to remove the given users permissions
     * @param userIdForPermissions user to remove permissions
     * @param permissions permissions to remove
     *
     * @throws FileNotFoundException if the given file doesn't exist
     */
    @Override
    public void removePermissionsForUser(String userId, String fileId, String userIdForPermissions, List<Permission> permissions) {
        UserFilesStorage userStorage = userFilesStorageService.findOrCreateUserFilesStorage(userId);
        File file = userStorage.findFileByFileId(fileId).orElseThrow(FileNotFoundException::new);
        file.removePermissionsForUser(userIdForPermissions, permissions);
        userFilesStorageService.save(userStorage);
    }
}
