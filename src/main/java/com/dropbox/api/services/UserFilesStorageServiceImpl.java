package com.dropbox.api.services;

import com.dropbox.api.exceptions.UserFileStorageNotFoundException;
import com.dropbox.api.models.File;
import com.dropbox.api.models.FileRequest;
import com.dropbox.api.models.UserFilesStorage;
import com.dropbox.api.storage.Storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.dropbox.api.models.File.createNewFile;
import static com.dropbox.api.models.FileType.FOLDER;
import static com.dropbox.api.models.UserFilesStorage.createNewStorage;

/**
 * Service to manipulate user file storage
 */
public class UserFilesStorageServiceImpl implements UserFilesStorageService {
    private final Storage storage;

    public UserFilesStorageServiceImpl(Storage storage) {
        this.storage = storage;
    }

    /**
     * Create a new empty user storage
     *
     * @param userId user identifier
     * @return create user storage
     */
    @Override
    public UserFilesStorage createUserStorage(String userId) {
        File rootFolder = createNewFile(null, FOLDER, "Dropbox", new ArrayList<>(), new HashMap<>());
        Map<String, File> folders = new HashMap<>();
        folders.put(rootFolder.getId(), rootFolder);
        UserFilesStorage userFilesStorage = createNewStorage(userId, rootFolder, folders);
        save(userFilesStorage);
        return userFilesStorage;
    }

    /**
     * Searches for the user storage or if it doesn't exist create new empty
     *
     * @param userId user identifier
     * @return user's files storage
     */
    @Override
    public UserFilesStorage findOrCreateUserFilesStorage(String userId) {
        try {
            return storage.findUserStorage(userId).orElseThrow(UserFileStorageNotFoundException::new);
        } catch (UserFileStorageNotFoundException e) {
            return createUserStorage(userId);
        }
    }

    /**
     * Add files to the given folder
     *
     * @param userId user identifier
     * @param folderId folder identifier to add the files
     * @param fileRequests files requests for adding
     * @return owner folder of the given added files
     */
    @Override
    public File addFilesToFolder(String userId, String folderId, List<FileRequest> fileRequests) {
        UserFilesStorage userStorage = findOrCreateUserFilesStorage(userId);
        List<File> files = fileRequests.stream()
                .map(file -> createNewFile(folderId, file.getType(), file.getName(), new ArrayList<>(),
                        file.getUsersPermission()))
                .collect(Collectors.toList());
        File parentFolder = userStorage.addFiles(folderId, files);
        save(userStorage);
        return parentFolder;
    }

    /**
     * Rename a file
     *
     * @param userId user identifier
     * @param fileId file identifier to rename
     * @param newName new name for the file
     * @return renamed file
     */
    @Override
    public File renameFile(String userId, String fileId, String newName) {
        UserFilesStorage userStorage = findOrCreateUserFilesStorage(userId);
        File file = userStorage.editFile(fileId, newName);
        save(userStorage);
        return file;
    }

    /**
     * Move file to another folder
     *
     * @param userId user identifier
     * @param fileId file identifier to move
     * @param toFolderId folder where to move the file
     * @return moved file
     */
    @Override
    public File moveFile(String userId, String fileId, String toFolderId) {
        UserFilesStorage userStorage = findOrCreateUserFilesStorage(userId);
        File file = userStorage.moveFile(fileId, toFolderId);
        save(userStorage);
        return file;
    }

    /**
     * Copy file to another folder
     *
     * @param userId user identifier
     * @param fileId file identifier to copy
     * @param toFolderId folder where to copy the file
     * @return copied file
     */
    @Override
    public File copyFile(String userId, String fileId, String toFolderId) {
        UserFilesStorage userStorage = findOrCreateUserFilesStorage(userId);
        File file = userStorage.copyFile(fileId, toFolderId);
        save(userStorage);
        return file;
    }

    /**
     * Remove files for the user
     *
     * @param userId user identifier
     * @param fileIds files identifiers to remove
     */
    @Override
    public void removeFiles(String userId, List<String> fileIds) {
        UserFilesStorage userStorage = findOrCreateUserFilesStorage(userId);
        userStorage.removeFiles(fileIds);
        save(userStorage);
    }

    /**
     * Save user's file storage
     *
     * @param userFilesStorage storage to save
     */
    @Override
    public void save(UserFilesStorage userFilesStorage) {
        storage.saveUserStorage(userFilesStorage);
    }
}
