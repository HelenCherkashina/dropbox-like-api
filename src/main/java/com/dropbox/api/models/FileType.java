package com.dropbox.api.models;

/**
 * types of files in system
 */
public enum FileType {
    DOCUMENT, FOLDER
}
