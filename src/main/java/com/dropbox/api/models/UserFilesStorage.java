package com.dropbox.api.models;

import com.dropbox.api.exceptions.FileNotFoundException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.dropbox.api.models.File.createNewFile;
import static com.dropbox.api.models.FileType.DOCUMENT;
import static com.dropbox.api.models.FileType.FOLDER;
import static java.util.Collections.unmodifiableMap;

/**
 * Representation of user storage of files and folders
 */
public class UserFilesStorage implements Searchable {
    private final String userId;
    private final File rootFolder;
    private final Map<String, File> storageFiles;

    private UserFilesStorage(String userId, File rootFolder, Map<String, File> storageFiles) {
        this.userId = userId;
        this.rootFolder = rootFolder;
        this.storageFiles = storageFiles;
    }

    /**
     * @return user unique identifier
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @return the main folder which contains all other files
     */
    public File getRootFolder() {
        return rootFolder;
    }

    /**
     * @return all storage files grouped by their id
     */
    public Map<String, File> getStorageFiles() {
        return unmodifiableMap(storageFiles);
    }

    /**
     * Search for a file by file id
     * @param fileId file identifier
     * @return optional of a found file
     */
    public Optional<File> findFileByFileId(String fileId) {
        return Optional.ofNullable(storageFiles.get(fileId));
    }

    /**
     * Add files to the given file
     *
     * @param folderId file identifier where to add files
     * @param files list of files to add
     * @return file where files were added
     *
     * @throws FileNotFoundException if file with the given id is not found
     * @throws UnsupportedOperationException if file with the given id is a terminal document
     */
    public File addFiles(String folderId, List<File> files) {
        File folder = findFileByFileId(folderId).orElseThrow(FileNotFoundException::new);

        if (folder.getType() != FOLDER) {
            throw new UnsupportedOperationException();
        }

        for(File file : files) {
            File newFile = folder.createNewFile(file.getType(), file.getName().getUploadedName(), file.getFiles(),
                    file.getUsersToShare());
            folder.getFiles().add(newFile);
            storageFiles.put(newFile.getId(), newFile);
        }

        return folder;
    }

    /**
     * Edit file name
     *
     * @param fileId file identifier to edit the name
     * @param name new name for the file
     * @return renamed file
     *
     * @throws FileNotFoundException if file with the given id is not found
     * or folder owner for the file is not found
     */
    public File editFile(String fileId, String name) {
        File file = findFileByFileId(fileId).orElseThrow(FileNotFoundException::new);
        File parentFolder = findFileByFileId(file.getParentFileId()).orElseThrow(FileNotFoundException::new);
        File fileWithNewName = parentFolder.editFile(file, name);
        storageFiles.put(fileId, fileWithNewName);
        return fileWithNewName;
    }

    /**
     * Move file from owner folder to the destination folder
     *
     * @param fileId file identifier for file to move
     * @param toFolderId new folder identifier where to move the file
     * @return moved file
     *
     * @throws FileNotFoundException if file with the given id is not found or destination folder is not found
     * @throws UnsupportedOperationException if destination folder doesn't have FOLDER type
     */
    public File moveFile(String fileId, String toFolderId) {
        File file = findFileByFileId(fileId).orElseThrow(FileNotFoundException::new);
        File fromFolder = findFileByFileId(file.getParentFileId()).orElseThrow(FileNotFoundException::new);
        File toFolder = findFileByFileId(toFolderId).orElseThrow(FileNotFoundException::new);

        if (toFolder.getType() != FOLDER) {
            throw new UnsupportedOperationException();
        }

        fromFolder.removeFile(fileId);
        storageFiles.remove(fileId);

        File newFile = toFolder.createNewFile(file.getType(), file.getName().getUploadedName(), file.getFiles(),
                file.getUsersToShare());
        toFolder.addFile(newFile);
        storageFiles.put(fileId, newFile);
        return newFile;
    }

    /**
     * Copy file to the destination folder
     *
     * @param fileId file identifier for file to copy
     * @param toFolderId new folder identifier where to copy the file
     * @return copied file
     *
     * @throws FileNotFoundException if file with the given id is not found or destination folder is not found
     * @throws UnsupportedOperationException if destination folder doesn't have FOLDER type
     */
    public File copyFile(String fileId, String toFolderId) {
        File file = findFileByFileId(fileId).orElseThrow(FileNotFoundException::new);
        File toFolder = findFileByFileId(toFolderId).orElseThrow(FileNotFoundException::new);

        if (toFolder.getType() != FOLDER) {
            throw new UnsupportedOperationException();
        }

        File newFile = toFolder.createNewFile(file.getType(), file.getName().getUploadedName(), file.getFiles(), file.getUsersToShare());
        toFolder.addFile(newFile);
        storageFiles.put(newFile.getId(), newFile);
        return newFile;
    }

    /**
     * Remove files from storage
     * @param fileIds files identifiers to remove
     */
    public void removeFiles(List<String> fileIds) {
        List<File> files = fileIds.stream()
                .map(id -> findFileByFileId(id).orElseThrow(FileNotFoundException::new))
                .collect(Collectors.toList());

        files.forEach(file -> findFileByFileId(file.getParentFileId()).orElseThrow(FileNotFoundException::new).removeFile(file.getId()));
        fileIds.forEach(storageFiles::remove);
    }

    /**
     * Search for the file with the given name within the storage
     * @param fileName file name to search
     * @return list of the matched files
     */
    @Override
    public List<File> searchByFileName(String fileName) {
        return storageFiles.values().stream()
                .filter(file -> file.getName().getGeneratedName().contains(fileName))
                .collect(Collectors.toList());
    }

    /**
     * Search for the file with the given identifier
     * @param id file identifier to search
     * @return optional of searched file
     */
    @Override
    public Optional<File> searchByFileId(String id) {
        return Optional.ofNullable(storageFiles.get(id));
    }

    public static UserFilesStorage createNewStorage(String userId, File rootFolder, Map<String, File> storageFiles) {
        return new UserFilesStorage(userId, rootFolder, storageFiles);
    }
}
