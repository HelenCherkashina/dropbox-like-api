package com.dropbox.api.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.dropbox.api.models.FileName.createFileName;
import static java.util.Collections.unmodifiableMap;

/**
 * A representation of simple file or folder data
 */
public class File implements Sharable {
    private final String id;
    private final FileType type;
    private final String parentFileId;
    private final FileName name;
    private final List<File> files;
    private final Map<String, List<Permission>> usersPermissions;

    private File(String id, FileType type, String parentFileId, FileName name, List<File> files, Map<String, List<Permission>> usersPermissions) {
        this.id = id;
        this.type = type;
        this.parentFileId = parentFileId;
        this.name = name;
        this.files = files;
        this.usersPermissions = usersPermissions;
    }

    /**
     * Represents an unique identifier of file
     * @return unique identifier of file
     */
    public String getId() {
        return id;
    }

    /**
     * The type of the given file. Can be either DOCUMENT or FOLDER
     * @see FileType
     * @return type of the given file
     */
    public FileType getType() {
        return type;
    }

    /**
     * Each file should contain the owner file id. For root folder the value should be null
     * @return owner file id
     */
    public String getParentFileId() {
        return parentFileId;
    }

    /**
     * Represents name of the file
     * @return name of file
     */
    public FileName getName() {
        return name;
    }

    /**
     * Returns list of inner files which the file contains.
     * Should be possible only for files with FOLDER type.
     * For DOCUMENT type should be empty
     * @return
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * Search for files in the current folder by term
     *
     * @param name file name search term
     * @return list of files that match the search term
     */
    public List<File> searchByName(String name) {
        return files.stream()
                .filter(file -> file.getName().getGeneratedName().contains(name))
                .collect(Collectors.toList());
    }

    /**
     * @return users those can see the file with their permissions
     */
    @Override
    public Map<String, List<Permission>> getUsersToShare() {
        return unmodifiableMap(usersPermissions);
    }

    /**
     * Add permissions for the given user to the current file
     * @param userId user identifier which will have access to the file
     * @param permissions list of permissions for the user
     */
    @Override
    public void addUserToShare(String userId, List<Permission> permissions) {
        usersPermissions.put(userId, permissions);
    }

    /**
     * Disallow users to access the file
     * @param usersIds list of users
     */
    @Override
    public void removeUsersToShare(List<String> usersIds) {
        usersIds.forEach(usersPermissions::remove);
    }

    /**
     * Remove given permissions from the current file for the user
     * @param userId user identifier which has access to the file
     * @param permissions list of permissions to remove for the user
     */
    @Override
    public void removePermissionsForUser(String userId, List<Permission> permissions) {
        List<Permission> existingPermissions = usersPermissions.get(userId);
        if(existingPermissions != null) {
            existingPermissions.removeAll(permissions);
        }
    }

    File createNewFile(FileType type, String name, List<File> files, Map<String, List<Permission>> usersPermissions) {
        return new File(UUID.randomUUID().toString(), type, this.getId(), this.generateFileName(type, name), files,
                new HashMap<>(usersPermissions));
    }

    File editFile(File file, String name) {
        removeFile(file.getId());
        File fileWithNewName = new File(file.getId(), file.getType(), this.id, this.generateFileName(file.getType(), name), file.getFiles(),
                file.getUsersToShare());
        addFile(fileWithNewName);
        return fileWithNewName;
    }

    void addFile(File newFile) {
        files.removeIf(file -> file.getId().equals(newFile.getId()));
        files.add(newFile);
    }

    void removeFile(String fileId) {
        files.removeIf(file -> file.getId().equals(fileId));
    }

    private FileName generateFileName(FileType type, String name) {
        long existingFilesWithSuchNameCount = files.stream()
                .filter(file -> type == file.getType())
                .filter(file -> file.getName().getUploadedName().equals(name)).count();
        return FileName.generateName(name, existingFilesWithSuchNameCount);
    }

    public static File createNewFile(String parentFileId, FileType type, String name, List<File> files, Map<String, List<Permission>> usersPermissions) {
        return new File(UUID.randomUUID().toString(), type, parentFileId, createFileName(name), files, usersPermissions);
    }
}
