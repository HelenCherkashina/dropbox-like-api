package com.dropbox.api.models;

import java.util.List;
import java.util.Map;

/**
 * Sharable resource which may contain permissions and manage them
 */
public interface Sharable {
    /**
     * @return list of users with permissions that have access to the resource
     */
    Map<String, List<Permission>> getUsersToShare();

    /**
     * Add access to the user with the given permissions to the resource
     *
     * @param userId      user identifier which will have access to the file
     * @param permissions list of permissions for the user
     */
    void addUserToShare(String userId, List<Permission> permissions);

    /**
     * Remove access to resource for the given users
     *
     * @param usersIds list of users
     */
    void removeUsersToShare(List<String> usersIds);

    /**
     * Remove permissions for the given user for resource
     *
     * @param userId      user identifier which has access to the file
     * @param permissions list of permissions to remove for the user
     */
    void removePermissionsForUser(String userId, List<Permission> permissions);
}
