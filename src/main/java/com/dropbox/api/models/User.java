package com.dropbox.api.models;

import java.util.UUID;

/**
 * Representation of a system user
 */
public class User {
    private final String userId;
    private final String email;
    private final String password;

    private User(String userId, String email, String password) {
        this.userId = userId;
        this.email = email;
        this.password = password;
    }

    /**
     * @return user unique identifier
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @return user email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return user password
     */
    public String getPassword() {
        return password;
    }

    public static User createNewUser(String email, String password) {
        return new User(UUID.randomUUID().toString(), email, password);
    }
}
