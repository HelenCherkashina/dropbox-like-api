package com.dropbox.api.models;

import java.util.List;
import java.util.Optional;

/**
 * Searchable resource
 */
public interface Searchable {
    /**
     * Search for the file with the given name within the storage
     * @param fileName file name to search
     * @return list of the matched files
     */
    List<File> searchByFileName(String fileName);

    /**
     * Search for the file with the given identifier
     * @param id file identifier to search
     * @return optional of searched file
     */
    Optional<File> searchByFileId(String id);
}
