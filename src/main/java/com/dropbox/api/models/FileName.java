package com.dropbox.api.models;

/**
 * Representation of a file name
 */
public class FileName {
    private static final String GENERATED_FILE_NAME_TEMPLATE = "%s(%s)";

    private final String generatedName;
    private final String uploadedName;

    private FileName(String generatedName, String uploadedName) {
        this.generatedName = generatedName;
        this.uploadedName = uploadedName;
    }

    /**
     * @return name of a file generated automatically
     */
    public String getGeneratedName() {
        return generatedName;
    }

    /**
     * @return original name of a file set when the file was uploaded
     */
    public String getUploadedName() {
        return uploadedName;
    }

    static FileName generateName(String fileName, long existingFilesWithSuchNameCount) {
        return existingFilesWithSuchNameCount == 0 ?
                createFileName(fileName) :
                createFileName(String.format(GENERATED_FILE_NAME_TEMPLATE, fileName, existingFilesWithSuchNameCount), fileName);
    }

    static FileName createFileName(String name) {
        return new FileName(name, name);
    }

    static FileName createFileName(String generatedName, String uploadedName) {
        return new FileName(generatedName, uploadedName);
    }
}
