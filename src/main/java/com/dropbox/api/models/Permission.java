package com.dropbox.api.models;

/**
 * Types of permissions in system
 */
public enum Permission {
    VIEW, UPDATE
}
