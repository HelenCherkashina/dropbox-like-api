package com.dropbox.api.models;

import java.util.List;
import java.util.Map;

public class FileRequest {
    private final FileType type;
    private final String name;
    private final Map<String, List<Permission>> usersPermission;

    private FileRequest(FileType type, String name, Map<String, List<Permission>> usersPermission) {
        this.type = type;
        this.name = name;
        this.usersPermission = usersPermission;
    }

    public FileType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public Map<String, List<Permission>> getUsersPermission() {
        return usersPermission;
    }

    public static FileRequest createFileRequest(FileType type, String name, Map<String, List<Permission>> usersPermission) {
        return new FileRequest(type, name, usersPermission);
    }
}
